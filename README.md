# SevenTeam Libraries

## Developed by:
- **Dang Thanh Tung** [dtt.dangthanhtung@gmail.com]
- **Tran Xuan Khanh** [xuankhanhyb@gmail.com]
- **Duong Ngo** [ngoducduonglucky@gmail.com]
- **Hau Chu** [hauchu1196@gmail.com]
- **Nguyen Chi Cong** [congdota@gmail.com]
- **Tran Van Cu** [trailangtien1996@gmail.com]

### How to build?
- Required:
    - Java JDK 8
    - Maven
    
- Tool:
    - Eclipse IDE for Java EE Developers

- Open Git Bash or Command Prompt (on Window) or Terminal (on Ubuntu)

- Clone project:
```
git clone https://gitlab.com/seventeamvietnam/seventeam-libs.git
```