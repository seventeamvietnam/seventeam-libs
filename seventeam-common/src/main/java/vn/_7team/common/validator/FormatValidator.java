/*******************************************************
 * Copyright 2018 by SevenTeam - All rights reserved.  *    
 *******************************************************/
package vn._7team.common.validator;

import java.util.regex.Matcher;

import vn._7team.common.FormatConstant;
import vn._7team.common.exception.SevenTeamException;
import vn._7team.common.util.StringUtil;

/**
 * @author  Dang Thanh Tung 
 * {@literal <dtt.dangthanhtung@gmail.com>}
 * @since   11/03/2018
 */
public abstract class FormatValidator {

  public void validateEmailField(String value) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException();
    Matcher matcher = FormatConstant.EMAIL_PATTERN.matcher(value.trim());
    if(matcher.matches()) return;
    throw new SevenTeamException.InvalidFormatDataException();
  }

  public void validateEmailField(String value, String message) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException(message);
    Matcher matcher = FormatConstant.EMAIL_PATTERN.matcher(value.trim());
    if(matcher.matches()) return;
    throw new SevenTeamException.InvalidFormatDataException(message);
  }

  public String validatePhoneField(String value) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException();
    String phone = PhoneValidator.validPhoneNumber(value);
    if(phone == null) throw new SevenTeamException.InvalidFormatDataException();
    return phone;
  }

  public String validatePhoneField(String value, String message) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException(message);
    String phone = PhoneValidator.validPhoneNumber(value);
    if(phone == null) throw new SevenTeamException.InvalidFormatDataException(message);
    return phone;
  }

  public void validateUUIDField(String value) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException();
    Matcher matcher = FormatConstant.UUID_PATTERN.matcher(value.trim());
    if(matcher.matches()) return;
    throw new SevenTeamException.InvalidFormatDataException();
  }

  public void validateUUIDField(String value, String message) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException(message);
    Matcher matcher = FormatConstant.UUID_PATTERN.matcher(value.trim());
    if(matcher.matches()) return;
    throw new SevenTeamException.InvalidFormatDataException(message);
  }

  public void validateOTPField(String value) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException();
    Matcher matcher = FormatConstant.OTP_PATTERN.matcher(value.trim());
    if(matcher.matches()) return;
    throw new SevenTeamException.InvalidFormatDataException();
  }

  public void validateOTPField(String value, String message) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException(message);
    Matcher matcher = FormatConstant.OTP_PATTERN.matcher(value.trim());
    if(matcher.matches()) return;
    throw new SevenTeamException.InvalidFormatDataException(message);
  }

}
