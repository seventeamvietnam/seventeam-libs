/*******************************************************
 * Copyright 2017 by SevenTeam - All rights reserved.  *    
 *******************************************************/
package vn._7team.common;

/**
 * @author  Dang Thanh Tung 
 * {@literal <dtt.dangthanhtung@gmail.com>}
 * @since   08/11/2017
 */
public class CommonConstant {

  public static final int DEFAULT_PAGE_NUMBER = 1;
  public static final int DEFAULT_PAGE_SIZE = 10;

}
