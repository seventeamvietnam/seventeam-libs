/*******************************************************
 * Copyright 2017 by SevenTeam - All rights reserved.  *    
 *******************************************************/
package vn._7team.common.validator;

import java.util.Date;

import vn._7team.common.exception.SevenTeamException;
import vn._7team.common.util.DateUtil;
import vn._7team.common.util.StringUtil;

/**
 * @author  Dang Thanh Tung 
 * {@literal <dtt.dangthanhtung@gmail.com>}
 * @since   12/11/2017
 */
public abstract class InputValidator extends FormatValidator {

  public void validateEmptyField(String value) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException();
  }

  public void validateEmptyField(String value, String message) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException(message);
  }

  public void validateEmptyField(Integer value) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
  }

  public void validateEmptyField(Integer value, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
  }

  public void validateEmptyField(Long value) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
  }

  public void validateEmptyField(Long value, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
  }

  public void validateEmptyField(Short value) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
  }

  public void validateEmptyField(Short value, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
  }

  public void validateEmptyField(Double value) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
  }

  public void validateEmptyField(Double value, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
  }

  public void validateEmptyField(Float value) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
  }

  public void validateEmptyField(Float value, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
  }

  public void validateIntegerField(String value) throws SevenTeamException {
    if(StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException();
    char [] chars = value.toCharArray();
    for(char c : chars) {
      if(Character.isDigit(c) || c == '_' || c == ',') continue;
      throw new SevenTeamException.InvalidFormatDataException();
    }
  }

  public void validateIntegerField(String value, String message) throws SevenTeamException {
    if(StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException(message);
    char [] chars = value.toCharArray();
    for(char c : chars) {
      if(Character.isDigit(c) || c == '_' || c == ',') continue;
      throw new SevenTeamException.InvalidFormatDataException(message);
    }
  }

  public void validateFloatField(String value) throws SevenTeamException {
    if(StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException();
    char [] chars = value.toCharArray();
    for(char c : chars) {
      if(Character.isDigit(c) || c == '_' || c == ',' || c == '.') continue;
      throw new SevenTeamException.InvalidFormatDataException();
    }
  }

  public void validateFloatField(String value, String message) throws SevenTeamException {
    if(StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException(message);
    char [] chars = value.toCharArray();
    for(char c : chars) {
      if(Character.isDigit(c) || c == '_' || c == ',' || c == '.') continue;
      throw new SevenTeamException.InvalidFormatDataException(message);
    }
  }

  public Date validateDateTimeField(String value, String format) throws SevenTeamException {
    if(StringUtil.isEmpty(value) || StringUtil.isEmpty(format)) throw new SevenTeamException.NoDataException();
    Date date = DateUtil.toDate(value, format);
    if(date == null) throw new SevenTeamException.InvalidFormatDataException();
    return date;
  }

  public Date validateDateTimeField(String value, String format, String message) throws SevenTeamException {
    if (StringUtil.isEmpty(value) || StringUtil.isEmpty(format)) throw new SevenTeamException.NoDataException(message);
    Date date = DateUtil.toDate(value, format);
    if(date == null) throw new SevenTeamException.InvalidFormatDataException(message);
    return date;
  }

  public void validateRangeField(Long value, long min, long max) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    long data = value.longValue();
    if(data < min || data > max) throw new SevenTeamException.InvalidDataException();
  }

  public void validateRangeField(Long value, long min, long max, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    long data = value.longValue();
    if(data < min || data > max) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateRangeField(Integer value, int min, int max) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    int data = value.intValue();
    if(data < min || data > max) throw new SevenTeamException.InvalidDataException();
  }

  public void validateRangeField(Integer value, int min, int max, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    int data = value.intValue();
    if(data < min || data > max) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateRangeField(Short value, short min, short max) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    short data = value.shortValue();
    if(data < min || data > max) throw new SevenTeamException.InvalidDataException();
  }

  public void validateRangeField(Short value, short min, short max, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    short data = value.shortValue();
    if(data < min || data > max) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateRangeField(Date value, Date min, Date max) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    if(value.before(min) || value.after(max)) throw new SevenTeamException.InvalidDataException();
  }

  public void validateRangeField(Date value, Date min, Date max, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    if(value.before(min) || value.after(max)) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateRangeLengthField(String value, int minLength, int maxLength) throws SevenTeamException {
    if(value.length() >= minLength && value.length() <= maxLength) return;
    throw new SevenTeamException.InvalidDataException("Invalid Range Length (" + minLength + ", " + maxLength + ")");
  }

  public void validateRangeLengthField(String value, int minLength, int maxLength, String message) throws SevenTeamException {
    if(value.length() >= minLength && value.length() <= maxLength) return;
    throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMinField(Long value, long min) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    long data = value.longValue();
    if(data < min) throw new SevenTeamException.InvalidDataException();
  }

  public void validateMinField(Long value, long min, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    long data = value.longValue();
    if(data < min) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMinField(Integer value, int min) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    int data = value.intValue();
    if(data < min) throw new SevenTeamException.InvalidDataException();
  }

  public void validateMinField(Integer value, int min, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    int data = value.intValue();
    if(data < min) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMinField(Short value, short min) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    short data = value.shortValue();
    if(data < min) throw new SevenTeamException.InvalidDataException();
  }

  public void validateMinField(Short value, short min, String message) throws SevenTeamException {
    if(value == null) throw new SevenTeamException.NoDataException(message);
    short data = value.shortValue();
    if(data < min) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMinField(Date value, Date min) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    if(min.after(value)) throw new SevenTeamException.InvalidDataException();
  }

  public void validateMinField(Date value, Date min, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    if(min.after(value)) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMinLengthField(String value, int minLength) throws SevenTeamException {
    if(value.length() >= minLength) return;
    throw new SevenTeamException.InvalidDataException("Invalid Min Length (" + minLength + ")");
  }

  public void validateMinLengthField(String value, int minLength, String message) throws SevenTeamException {
    if(value.length() >= minLength) return;
    throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMaxField(Long value, long max) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    long data = value.longValue();
    if(data > max) throw new SevenTeamException.InvalidDataException();
  }

  public void validateMaxField(Long value, long max, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    long data = value.longValue();
    if(data > max) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMaxField(Integer value, int max) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    int data = value.intValue();
    if(data > max) throw new SevenTeamException.InvalidDataException();
  }

  public void validateMaxField(Integer value, int max, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    int data = value.intValue();
    if(data > max) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMaxField(Short value, short max) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    short data = value.shortValue();
    if(data > max) throw new SevenTeamException.InvalidDataException();
  }

  public void validateMaxField(Short value, short max, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    short data = value.shortValue();
    if(data > max) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMaxField(Date value, Date max) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    if(value.after(max)) throw new SevenTeamException.InvalidDataException();
  }

  public void validateMaxField(Date value, Date max, String message) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    if(value.after(max)) throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateMaxLengthField(String value, int maxLength) throws SevenTeamException {
    if(value.length() <= maxLength) return;
    throw new SevenTeamException.InvalidDataException("Invalid Max Length (" + maxLength + ")");
  }

  public void validateMaxLengthField(String value, int maxLength, String message) throws SevenTeamException {
    if(value.length() <= maxLength) return;
    throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateSomeValuesField(Long value, long...values) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    long temp = value.longValue();
    for(long ele : values) {
      if(temp == ele) return;
    }
    throw new SevenTeamException.InvalidDataException();
  }

  public void validateSomeValuesField(Long value, String message, long...values) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    long temp = value.longValue();
    for(long ele : values) {
      if(temp == ele) return;
    }
    throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateSomeValuesField(Integer value, int...values) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    int temp = value.intValue();
    for(int ele : values) {
      if(ele == temp) return;
    }
    throw new SevenTeamException.InvalidDataException();
  }

  public void validateSomeValuesField(Integer value, String message, int...values) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    int temp = value.intValue();
    for(int ele : values) {
      if(ele == temp) return;
    }
    throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateSomeValuesField(Short value, short...values) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException();
    short temp = value.shortValue();
    for(short ele : values) {
      if(ele == temp) return;
    }
    throw new SevenTeamException.InvalidDataException();
  }

  public void validateSomeValuesField(Short value, String message, short...values) throws SevenTeamException {
    if (value == null) throw new SevenTeamException.NoDataException(message);
    short temp = value.shortValue();
    for(short ele : values) {
      if(ele == temp) return;
    }
    throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateSomeValuesField(String value, String...values) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException();
    for(String ele : values) {
      if(value.equals(ele)) return;
    }
    throw new SevenTeamException.InvalidDataException();
  }

  public void validateSomeValuesField(String value, String message, String...values) throws SevenTeamException {
    if (StringUtil.isEmpty(value)) throw new SevenTeamException.NoDataException(message);
    for(String ele : values) {
      if(value.equals(ele)) return;
    }
    throw new SevenTeamException.InvalidDataException(message);
  }

  public void validateSomeValuesField(Character value, char...values) throws SevenTeamException {
    if(value == null) throw new SevenTeamException.NoDataException();
    char temp = value.charValue();
    for(char ele : values) {
      if(ele == temp) return;
    }
    throw new SevenTeamException.InvalidDataException();
  }

  public void validateSomeValuesField(Character value, String message, char...values) throws SevenTeamException {
    if(value == null) throw new SevenTeamException.NoDataException(message);
    char temp = value.charValue();
    for(char ele : values) {
      if(ele == temp) return;
    }
    throw new SevenTeamException.InvalidDataException(message);
  }

}
