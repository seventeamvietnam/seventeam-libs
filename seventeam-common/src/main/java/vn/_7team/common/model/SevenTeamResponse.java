/*******************************************************
 * Copyright 2017 by SevenTeam - All rights reserved.  *    
 *******************************************************/
package vn._7team.common.model;

import java.io.Serializable;

import vn._7team.common.util.JsonUtil;

/**
 * @author  Dang Thanh Tung 
 * {@literal <dtt.dangthanhtung@gmail.com>}
 * @since   13/09/2017
 */
@SuppressWarnings("serial")
public class SevenTeamResponse<T> implements Serializable {

  protected int code;
  protected String message;
  protected T data;

  public SevenTeamResponse() {}

  public SevenTeamResponse(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public SevenTeamResponse(int code, String message, T data) {
    this.code = code;
    this.message = message;
    this.data = data;
  }

  public int getCode() { return code; }
  public void setCode(int code) { this.code = code; }

  public String getMessage() { return message; }
  public void setMessage(String message) { this.message = message; }

  public T getData() { return data; }
  public void setData(T data) { this.data = data; }

  @Override
  public String toString() {
    return JsonUtil.toJson(this);
  }

}
