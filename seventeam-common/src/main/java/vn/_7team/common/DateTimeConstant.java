/*******************************************************
 * Copyright 2018 by SevenTeam - All rights reserved.  *    
 *******************************************************/
package vn._7team.common;

/**
 * @author  Dang Thanh Tung 
 * {@literal <dtt.dangthanhtung@gmail.com>}
 * @since   07/01/2018
 */
public class DateTimeConstant {

  public static final String HHmmss = "HH:mm:ss";

  public static final String ddMMyyyy = "dd/MM/yyyy";
  public static final String ddMMyyyyHHmmss = "dd/MM/yyyy HH:mm:ss";
  public static final String HHmmssddMMyyyy = "HH:mm:ss dd/MM/yyyy";

  public static final String yyyyMMdd = "yyyy-MM-dd";
  public static final String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
  public static final String HHmmssyyyyMMdd = "HH:mm:ss yyyy-MM-dd";

  public static final String ddMMHHmm = "dd/MM HH:mm";
  public static final String HHmmddMM = "HH:mm dd/MM";

  public static final String MMddHHmm = "MM-dd HH:mm";
  public static final String HHmmMMdd = "HH:mm MM-dd";

}
